import Vue from 'vue';
import Vuex from 'vuex';

import parseFile from '@/utils/fileParser';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		selectedFile: null,
		fileContent: null,
		parsedFileContent: null,
		comments: [],
		username: localStorage.getItem('CodeReview-username'),
		linesSelected: [],
		linesSelectedMetadata: {},
	},
	mutations: {
		setFile(state, { file, textContent, parsedContent }) {
			state.selectedFile = file;
			state.fileContent = textContent;
			state.parsedFileContent = parsedContent;
		},
		setComments(state, comments) {
			state.comments = comments;
		},
		setUsername(state, username) {
			localStorage.setItem('CodeReview-username', username);
			state.username = username;
		},
		setLinesSelected(state, { selected, metadata }) {
			state.linesSelected = selected;
			state.linesSelectedMetadata = metadata;
		},
		addComment(state, comment) {
			state.comments.push(comment);
		},
		addReply(state, { reply, commentId }) {
			state.comments.find(c => c.id === commentId).replies.push(reply);
		},
	},
	actions: {
		fileChosen({ commit }, { file, textContent }) {
			const parsed = parseFile(textContent);
			commit('setFile', { file, textContent, parsedContent: parsed });
		},
		onClickLine({ state, commit }, { $event, fileName, hunkIdx, lineIdx }) {
			let selected;
			let metadata;
			if ($event.shiftKey && state.linesSelected.length && state.linesSelectedMetadata.fileName === fileName && state.linesSelectedMetadata.hunkIdx === hunkIdx) {
				const min = Math.min(state.linesSelected[0], lineIdx);
				const max = Math.max(state.linesSelected[state.linesSelected.length - 1], lineIdx);
				selected = [min];
				for (let line = min + 1; line <= max; line++) {
					selected.push(line);
				}
				metadata = state.linesSelectedMetadata;
			} else if (state.linesSelected.length === 1 && state.linesSelected[0] == lineIdx) {
				selected = [];
				metadata = {};
			} else {
				selected = [lineIdx];
				metadata = { fileName, hunkIdx };
			}
			commit('setLinesSelected', { selected, metadata });
		},
	},
	getters: {
		getSnippet: state => ({ fileName, hunkIdx, linesSelected, trimWhitespace = false }) => {
			const allLines = state.parsedFileContent[fileName][hunkIdx].lines;
			let selectedLines = linesSelected.map(line => allLines[line]);
			if (trimWhitespace) {
				let commonWhitespace = 1;
				let searching = true;
				while (searching) {
					if (selectedLines.every(line => [' ', '\t'].includes(line[commonWhitespace]))) commonWhitespace++;
					else searching = false;
				}
				selectedLines = selectedLines.map(line => line[0] + line.slice(commonWhitespace));
			}
			return selectedLines.join('\n');
		},
		selectedSnippet(state, getters) {
			if (state.linesSelected.length) {
				return getters.getSnippet({
					...state.linesSelectedMetadata,
					linesSelected: state.linesSelected,
					trimWhitespace: true,
				});
			}
			return '';
		},
	},
	modules: {},
});
