export default function parseFile(fileContent) {
	const files = {};
	let parsingFile = null;
	let parsingHunk = null;
	for (const line of fileContent.trimEnd().split('\n')) {
		if (line.startsWith('diff --git')) {
			const filename = line.split(' ')[2].slice(2);
			parsingFile = filename;
			parsingHunk = null;
			files[filename] = [];
		} else if (parsingFile) {
			if (line.startsWith('@@')) {
				const [, lineStartA, lineStartB] = line.match(/@@ -(\d+),\d+ \+(\d+),\d+ @@(?: .+)?/);
				parsingHunk = {
					lineStartA: Number(lineStartA) + 1,
					lineStartB: Number(lineStartB) + 1,
					lines: [],
				};
				files[parsingFile].push(parsingHunk);
			} else if (parsingHunk) {
				parsingHunk.lines.push(line);
			} else {
				// ignore
			}
		} else {
			// ignore
		}
	}
	return files;
}
