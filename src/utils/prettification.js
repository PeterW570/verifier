export function prettyDate(date) {
	return new Date(date).toLocaleString();
}

export function relativeTime(date) {
	const current = new Date();
	const previous = new Date(date);

	const msPerMinute = 60 * 1000;
	const msPerHour = msPerMinute * 60;
	const msPerDay = msPerHour * 24;
	const msPerMonth = msPerDay * 30;
	const msPerYear = msPerDay * 365;

	const elapsed = current - previous;

	let value;
	let unit;

	if (elapsed < msPerMinute) {
		value = Math.round(elapsed / 1000);
		unit = 'second';
	} else if (elapsed < msPerHour) {
		value = Math.round(elapsed / msPerMinute);
		unit = 'minute';
	} else if (elapsed < msPerDay) {
		value = Math.round(elapsed / msPerHour);
		unit = 'hour';
	} else if (elapsed < msPerMonth) {
		value = Math.round(elapsed / msPerDay);
		unit = 'day';
	} else if (elapsed < msPerYear) {
		value = Math.round(elapsed / msPerMonth);
		unit = 'month';
	} else {
		value = Math.round(elapsed / msPerYear);
		unit = 'year';
	}

	return `${value} ${unit}${value !== 1 ? 's' : ''} ago`;
}
