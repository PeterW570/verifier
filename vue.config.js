const webpack = require('webpack');

module.exports = {
	pwa: {
		name: 'CodeReview',
	},
	configureWebpack: {
		plugins: [
			new webpack.DefinePlugin({
				'process.env': {
					VERSION: JSON.stringify(require('./package.json').version),
				},
			}),
		],
	},
	chainWebpack: config => {
		config.plugin('html').tap(args => {
			args[0].title = 'CodeReview';
			return args;
		});
	},
};
