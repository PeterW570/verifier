module.exports = {
	singleQuote: true,
	trailingComma: 'es5',
	semi: true,
	printWidth: 200,
	tabWidth: 4,
	useTabs: true,
};
