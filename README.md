# code-review

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Run your end-to-end tests

```
npm run test:e2e
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

# TODO:

-   multiline selection on touchscreen by long-pressing
-   dark mode
-   way to tick off hunks are they're reviewed?
-   way to easily comment on a hunk?
-   ability to collapse a hunk
-   a way to share a url instead of having to export/import?
